/**
 * @file    TcpClient.h
 * @brief   TcpClient interface.
 *
 * @author Jaroslav Petrovsky
 * @date   18. April 2015
 */

#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include "BotApplication.h"

#include <QObject>
#include <QTcpSocket>


class TcpClient : public QObject
{
    Q_OBJECT

    public:
        TcpClient( QObject *parent=0);
        ~TcpClient();

        // General methods
        void handshake(QString hostName, quint16 port);

        // signals
        Q_SIGNAL void error(ErrorLevel errorLevel, QString message);
        Q_SIGNAL void messageReceieved(QByteArray message);

        // slots
        Q_SLOT void processSocketError(QAbstractSocket::SocketError socketError);
        Q_SLOT void readSocketData();

        Q_SLOT void sendGameUpdate(QByteArray message);
        Q_SLOT void sendLoginData(QByteArray message);


    private:
        void sendMessage(QByteArray message);
        Q_SLOT void reconnect();

        // members
        QTcpSocket* _socket;
        QString     _hostName;
        quint16     _port;
};

#endif // TCP_CLIENT_H

