/**
 * @file    Conflict.cpp
 * @brief   Class for evaluating bot conflicts.
 *
 * @author Jaroslav Petrovsky
 * @date   02. May 2015
 *
 */

#include "Conflict.h"

#include <cmath>

Point::Point(double x, double y) :
    _x(x), _y(y)
{
}

Point Point::operator-(const Point& point)
{
    return Point(_x - point._x, _y - point._y);
}

Point Point::operator+(const Point &point)
{
    return Point(_x + point._x, _y + point._y);
}

// ---------------------------------------------------------------

// credits to http://devmag.org.za/2009/04/17/basic-collision-detection-in-2d-part-2/
int Conflict::circleLineCollision(Point linePoint1, Point linePoint2, Point circleCenter, double circleRadius)
{
    // transform to local
    Point localP1 = linePoint1 - circleCenter;
    Point localP2 = linePoint2 - circleCenter;

    // precalculate - often used
    Point P2MinusP1 = localP2 - localP1;

    double a = (P2MinusP1.x()) * (P2MinusP1.x()) + (P2MinusP1.y()) * (P2MinusP1.y());
    double b = 2 * ((P2MinusP1.x() * localP1.x()) + (P2MinusP1.y() * localP1.y()));
    double c = (localP1.x() * localP1.x()) + (localP1.y() * localP1.y()) - (circleRadius * circleRadius);
    double delta = b * b - (4 * a * c);

    if (delta < 0) // No intersection
    {
        return 0;
    }
    else if (delta == 0) // One intersection
    {
        return 1;
    }
    else if (delta > 0) // Two intersections
    {
        return 2;
    }
}

int Conflict::circleCircleCollision(Point center1, double radius1, Point center2, double radius2)
{
    double centersDistance  = distance(center1, center2);
    double radiusSum        = radius1 + radius2;

    if(centersDistance > radiusSum)
    {
        return 0;
    }
    else if(centersDistance == radiusSum)
    {
        return 1;
    }
    else
    {
        return 2;
    }

}

// calculate distance of 2 points
double Conflict::distance(Point p1, Point p2)
{
    return sqrt((p1.x() - p2.x()) * (p1.x() - p2.x()) + (p1.y() - p2.y()) * (p1.y() - p2.y()));
}

Point Conflict::calculateLineEndPoint(Point from, double angle, double distance)
{
    double toX = from.x() + distance * cos(angle * M_PI/180);
    double toY = from.y() + distance * sin(angle * M_PI/180);

    return Point(toX, toY);
}

// angle of two vectors
double Conflict::angle(Point p1, Point p2)
{
    double dot = p1.x() * p2.x() + p1.y() * p2.y();
    double det = p1.x() * p2.y() - p1.y() * p2.x();

    // in radians
    double angle = atan2(det, dot);

    // return in degrees
    return angle * (180/M_PI);
}


