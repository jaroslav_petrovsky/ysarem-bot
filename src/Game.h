/**
 * @file    Game.h
 * @brief   Object that takes care of game
 *          setup and actual game (updades)

 *
 * @author Jaroslav Petrovsky
 * @date   21. April 2015
 */

#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QVector>

class QJsonObject;
class Bot;


class Game : public QObject
{
    Q_OBJECT

    public:
        Game( QObject* parent=0);
        ~Game();

        // general methods for game control
        void start(QString nickname, QMap<int, int> speedLevels);
        void update(const QJsonObject& jsonUpdate);
        void cleanUp();

        //signals
        Q_SIGNAL void sendCommand(QByteArray command);


    private:
        // members
        QVector<Bot*> _bots;

        bool    _running;
        QString _nickname;

        // constants
        const int NUMBER_OF_BOTS = 5;
};

#endif // GAME_H
