/**
 * @file    main.cpp
 * @brief   Application main function.
 *          TCP client and game initialization.
 *
 * @author  Jaroslav Petrovsky
 * @date    18. April 2015
 */

#include "BotApplication.h"

#include <QCoreApplication>

int main(int argc, char **argv)
{
    // qt application for event handling
    QCoreApplication app(argc, argv);

    // bot application for game processing and logic
    BotApplication botApplication(&app);

    return app.exec();
}

