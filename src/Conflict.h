/**
 * @file    Conflict.h
 * @brief   Class for evaluating bot conflicts
 *
 * @author Jaroslav Petrovsky
 * @date   02. May 2015
 *
 */

#ifndef CONFLICT_H
#define CONFLICT_H

class Point
{
    public:
        Point(double x, double y);

        Point operator-(const Point& point);
        Point operator+(const Point& point);

        double x() { return _x; }
        double y() { return _y; }

    private:
        double _x;
        double _y;
};

// ---------------------------------------------------------------

class Conflict
{
    public:
        static int circleLineCollision(
                    Point linePoint1,
                    Point linePoint2,
                    Point circleCenter,
                    double circleRadius
                );

        static int circleCircleCollision(
                    Point center1,
                    double radius1,
                    Point center2,
                    double radius2
                );

        static double distance(Point p1, Point p2);

        static Point calculateLineEndPoint(Point from, double angle, double distance);

        static double angle(Point p1, Point p2);
};

#endif // CONFLICT_H

