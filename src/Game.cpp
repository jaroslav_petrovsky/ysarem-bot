/**
 * @file    Game.cpp
 * @brief   Object that takes care of game
 *          setup and actual game (updades)
 *
 * @author Jaroslav Petrovsky
 * @date   21. April 2015
 */

#include "Game.h"

#include "Bot.h"

#include <QVector>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

Game::Game( QObject *parent )
    : QObject( parent ), _running(false)
{
}

Game::~Game()
{
}

// prepare game for new session
void Game::start(QString nickname, QMap<int, int> speedLevels)
{
    _nickname = nickname;

    // if the previous game havent been stopped properly
    if(_running == true)
    {
        for(int i = 0; i < _bots.length(); i++)
        {
            delete _bots[i];
        }
        _bots.clear();
    }

    // initialize bots
    for(int i = 1; i <= NUMBER_OF_BOTS; i++)
    {
        _bots.push_back(new Bot(i, _nickname, speedLevels));
    }

    _running = true;
}

// regular game updates
void Game::update(const QJsonObject& jsonUpdate)
{
    if(_running == true)
    {
        QJsonArray players = jsonUpdate["play"].toObject()["players"].toArray();

        // check if bot was alive previous update nad if yes, update
        for(int i = 0; i < _bots.length(); i++)
        {
            _bots[i]->update(players);
        }

        QJsonArray botsArray;
        // bots are updated, get update result, if alive
        for(int i = 0; i < _bots.length(); i++)
        {
            if(_bots[i]->alive() == true)
            {
                QJsonObject command = _bots[i]->getCommand();

                if(command.isEmpty() == false)
                {
                    botsArray.append(QJsonValue(command));
                }
            }
        }

        QJsonObject botsObject;

        botsObject.insert("bots", QJsonValue(botsArray));

        emit sendCommand(QJsonDocument(botsObject).toJson(QJsonDocument::Compact));
    }
}

// cleanup after game session
void Game::cleanUp()
{
    if(_running == true)
    {
        for(int i = 0; i < _bots.length(); i++)
        {
            delete _bots[i];
        }

        _bots.clear();

        _running = false;
    }
}
