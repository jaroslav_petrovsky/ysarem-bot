/**
 * @file    BotApplication.h
 * @brief   Main application that initializes all modules
 *          and takes care of error handling and communication
 *          between modules.
 *
 * @author Jaroslav Petrovsky
 * @date   18. April 2015
 */

#ifndef BOT_APPLICATION_H
#define BOT_APPLICATION_H

#include <QObject>
//#include <QTime>
#include <QJsonObject>
#include <QMutex>

class TcpClient;
class QCoreApplication;
class Game;

// enum that holds error levels
// fatal for error that shuts the bot, like connection to server error
// info for non fatal errors like not keeping the interval
enum class ErrorLevel
{
    FATAL,
    INFO
};

class BotApplication : public QObject
{
    Q_OBJECT

    public:
        BotApplication( QCoreApplication* parentApp);
        ~BotApplication();

    // General methods

    // signals
        Q_SIGNAL void sendLoginData(QByteArray message);
        Q_SIGNAL void sendCommand(QByteArray command);
        Q_SIGNAL void quit();

    // slots
        Q_SLOT void error(ErrorLevel errorLevel, QString errorMessage);
        Q_SLOT void receiveMessage(QByteArray message);
        Q_SLOT void processCommandFromGame(QByteArray command);

    private:
        QByteArray  generateHash(QString random);
        int         generateCmdId();
        void        resetApp();

        Q_SLOT void prepareCommand();

    // Members
        const QString   TOKEN       = "ee45fb0a-a0fd-4033-ad52-baf438e7dbb6";
        const QString   NICKNAME    = "Ysarem";
        const int       DELAY       = 200;      // in msec

        QCoreApplication*   _parentApp;
        TcpClient*          _tcpClient;
        Game*               _game;
        QJsonObject         _queuedCommand;
//        QTime               _delayTimer;
        bool                _isCommandInQueue = false;

        int                 _lastSentCmdId      = -1;
        int                 _lastProcessedCmdId = -1;

        QMutex              _commandLock;
};

#endif // BOT_APPLICATION_H

