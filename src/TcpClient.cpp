/**
 * @file    TcpClient.cpp
 * @brief   TcpClient implementation.
 *
 * @author Jaroslav Petrovsky
 * @date   18. April 2015
 */

#include "TcpClient.h"
#include "BotApplication.h"

#include <QHostAddress>

#include <iostream>
TcpClient::TcpClient( QObject *parent )
    : QObject( parent ), _socket(new QTcpSocket(this))
{
    // connections
    QObject::connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(processSocketError(QAbstractSocket::SocketError)));
    QObject::connect(_socket, SIGNAL(readyRead()), SLOT(readSocketData()));
    QObject::connect(_socket, SIGNAL(disconnected()), SLOT(reconnect()));
}

TcpClient::~TcpClient()
{
    if(_socket != nullptr)
    {
        delete _socket;
        _socket = nullptr;
    }
}

// initialize client and connect to server
void TcpClient::handshake(QString hostName, quint16 port)
{
    if(hostName.isEmpty() == false)
    {
        _hostName = hostName;
        _port     = port;

        reconnect();
    }
    else
    {
        emit error(ErrorLevel::FATAL, "Hostname supported at handshake is empty!");
    }

}

void TcpClient::processSocketError(QAbstractSocket::SocketError socketError)
{
    QString     msg;
    ErrorLevel  errorLevel = ErrorLevel::FATAL;

    switch(socketError)
    {
        case QAbstractSocket::HostNotFoundError:
            msg = "SOCKET_ERROR: The host was not found, check the host name and port settings.";
            break;
        case QAbstractSocket::ConnectionRefusedError:
            msg = "SOCKET_ERROR: The connection was refused by the peer.";
            break;
        case QAbstractSocket::RemoteHostClosedError:
            msg = "SOCKET_ERROR: " + _socket->errorString();
            errorLevel = ErrorLevel::INFO;
            break;
        default:
            msg = "SOCKET_ERROR: " + _socket->errorString();
    }

    emit error(errorLevel, msg);
}

void TcpClient::readSocketData()
{
    // if more than one update came, send just last one
    QString message = _socket->readAll();
    QStringList updates = message.split("\n");

    // do not send last elemet because if we get "something\n" in message
    // then last element is ""
    emit messageReceieved(updates[updates.size() - 2].toLatin1());
}

void TcpClient::sendGameUpdate(QByteArray message)
{
    sendMessage(message);
}

void TcpClient::sendLoginData(QByteArray message)
{
    sendMessage(message);
}

void TcpClient::sendMessage(QByteArray message)
{
    QString data = QString(message);

    std::cout << QString(message).toStdString() << std::endl;
    if(_socket != nullptr)
    {
        _socket->write(message);
    }
}

void TcpClient::reconnect()
{
    if(_socket != nullptr)
    {
        _socket->connectToHost(_hostName, _port);
    }
    else
    {
        QString msg = QString("Socket already created: %1:%2")
                        .arg(_socket->peerAddress().toString())
                        .arg(QString::number(_socket->peerPort()));

        emit error(ErrorLevel::INFO, msg);
    }
}
