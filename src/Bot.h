/**
 * @file Bot.h
 * @brief Bot logic
 *
 * @author Jaroslav Petrovsky
 * @date   21. April 2015
 */

#ifndef BOT_H
#define BOT_H

#include "Conflict.h"

#include <QObject>
#include <QJsonObject>
#include <QJsonArray>
#include <QMap>

class Bot : public QObject
{
    public:
        Bot(int id, QString nickname, QMap<int, int> speedLevels, QObject *parent = 0);

        void update(const QJsonArray& playersArray);
        QJsonObject getCommand();

        bool alive() const;

    private:
        void setAlive(bool alive);

        int defend(int enemyId);
        int getNearestTarget();
        int checkDangerZone();
        QJsonObject getEnemyBot(int id);

        double gameToGeometryAngle(double gameAngle);

    private:
        // bot vars
        int         _id;
        QString     _nickname;
        bool        _alive;
        QJsonObject _command;

        // match vars
        QJsonArray  _myBots;
        QJsonArray  _enemyBots;
        QJsonObject _myBot;

        int         _enemy;

        // game vars
        QMap<int, int> _speedLevels; // speed, maxAngle

        const int DANGER_ZONE_RADIUS = 500; // [px]
};

#endif // BOT_H

