/**
 * @file    Bot.cpp
 * @brief   Bot logic
 *
 * @author Jaroslav Petrovsky
 * @date   21. April 2015
 */

#include "Bot.h"

#include <QJsonValue>
#include <QJsonDocument>
#include <QVector>

#include <iostream>
#include <cmath>

Bot::Bot(int id, QString nickname, QMap<int, int> speedLevels, QObject* parent)
    : QObject(parent), _id(id), _alive(true), _nickname(nickname), _speedLevels(speedLevels)
{
}

void Bot::update(const QJsonArray& playersArray)
{
    if(_alive == false)
    {
        return;
    }

    // setup objects and check if still alive
    QJsonObject object = playersArray[0].toObject();

    if(object["nickname"].toString() == _nickname)
    {
        _myBots = object["bots"].toArray();
        _enemyBots = playersArray[1].toObject()["bots"].toArray();
    }
    else
    {
        _myBots = playersArray[1].toObject()["bots"].toArray();
        _enemyBots = object["bots"].toArray();
    }

    _alive = false;
    for(int i = 0; i < _myBots.size(); i++)
    {
        if(_myBots[i].toObject()["id"] == _id)
        {
            _alive = true;
            _myBot = _myBots[i].toObject();
        }
    }

    if(_alive == false)
    {
        // we are dead. farewell my friend, you fought well
        return;
    }

    // reset command
    _command = QJsonObject();

    // first, check if any enemy in danger zone
    int dangerousEnemy = checkDangerZone();

    if(dangerousEnemy != 0)
    {
        int isDefending = defend(dangerousEnemy);

        if(isDefending != 0)
        {
            // run away
            _command.insert("cmd", "accelerate");
        }

        if(_command.isEmpty() == false)
        {
            _command.insert("id", _id);
        }

        return;
    }

//    std::cout << "MY: " << QString(QJsonDocument(_myBot).toJson(QJsonDocument::Compact)).toStdString() << std::endl;
//    _command.insert("id", _id);
//    _command.insert("cmd", "steer");
//    _command.insert("angle", -30);

}

QJsonObject Bot::getCommand()
{
    return _command;
}

bool Bot::alive() const
{
    return _alive;
}

int Bot::getNearestTarget()
{

}

// return closest attacking enemy in danger zone
int Bot::checkDangerZone()
{
    // nearest attacking enemy in danger zone and his id
    int     enemyId     = 0;
    double  distance    = 0;

    Point botCenter(_myBot["x"].toDouble(), _myBot["y"].toDouble());

    // check every enemy
    for(int i = 0; i < _enemyBots.size(); i++)
    {
        // first, check if in the zone
        QJsonObject enemy = _enemyBots[i].toObject();
        Point enemyCenter(enemy["x"].toDouble(), enemy["y"].toDouble());

        int dangerZoneConflict = Conflict::circleCircleCollision(enemyCenter, 20, botCenter, DANGER_ZONE_RADIUS);

        if(dangerZoneConflict != 0)
        {
//            std::cout << "MY: " << QString(QJsonDocument(_myBot).toJson(QJsonDocument::Compact)).toStdString() << std::endl;
//            std::cout << "ENEMY: " << QString(QJsonDocument(enemy).toJson(QJsonDocument::Compact)).toStdString() << std::endl;

            // if in the zone, check if attacking
            double attackAngle      = gameToGeometryAngle(enemy["angle"].toDouble());
            Point attackEndPoint    = Conflict::calculateLineEndPoint(enemyCenter, attackAngle, DANGER_ZONE_RADIUS);

            int isAttacking = Conflict::circleLineCollision(enemyCenter, attackEndPoint, botCenter, 20);

//            if(isAttacking != 0)
            {
                // if attacking check current distance
                double enemyDistance = Conflict::distance(botCenter, enemyCenter);

                if(enemyId != 0 && distance != 0)
                {
                    // if smaller, remember
                    if(enemyDistance < distance)
                    {
                        distance = enemyDistance;
                        enemyId = enemy["id"].toInt();
                    }
                }
                else
                {
                    distance = enemyDistance;
                    enemyId = enemy["id"].toInt();
                }
            }
        }
    }

    return enemyId;
}

QJsonObject Bot::getEnemyBot(int id)
{
    QJsonObject enemyBot;

    for(int i = 0; i < _enemyBots.size(); i++)
    {
        enemyBot = _enemyBots[i].toObject();

        if(enemyBot["id"].toInt() == id)
        {
            return enemyBot;
        }
        else
        {
            enemyBot = QJsonObject();
        }
    }

    return enemyBot;
}

// convert game angle(headin EAST = 0, +/- 180) to geometry (0 - 360 anticlockwise)
double Bot::gameToGeometryAngle(double gameAngle)
{
    if(gameAngle > 0)
    {
        return 360 - gameAngle;
    }
    else if(gameAngle < 0)
    {
        return -(gameAngle);
    }
    else
    {
        return 0;
    }
}

void Bot::setAlive(bool alive)
{
    _alive = alive;
}

// return 0 if defending, or 1 if attacking bot too close to defend
int Bot::defend(int enemyId)
{
    // get the enemy bot
    QJsonObject enemyBot = getEnemyBot(enemyId);

    if(enemyBot.empty() == true)
    {
        return 0;
    }

    // check if facing towards
    Point myBotCenter(_myBot["x"].toDouble(), _myBot["y"].toDouble());
    Point enemyBotCenter(enemyBot["x"].toDouble(), enemyBot["y"].toDouble());

    double facingAngle  = gameToGeometryAngle(_myBot["angle"].toDouble());
    double botDistance = Conflict::distance(myBotCenter, enemyBotCenter);

    Point facingEndPoint = Conflict::calculateLineEndPoint(myBotCenter, facingAngle, botDistance);

    int isFacingTowardsBot = Conflict::circleLineCollision(myBotCenter, facingEndPoint, enemyBotCenter, 20);

    // not facing towards enemy
    if(isFacingTowardsBot == 0)
    {
//                    std::cout << "MY: " << QString(QJsonDocument(_myBot).toJson(QJsonDocument::Compact)).toStdString() << std::endl;
//                    std::cout << "ENEMY: " << QString(QJsonDocument(enemyBot).toJson(QJsonDocument::Compact)).toStdString() << std::endl;

        QString mybotinfo = QString(QJsonDocument(_myBot).toJson(QJsonDocument::Compact));
        QString enemyBotInf0 = QString(QJsonDocument(enemyBot).toJson(QJsonDocument::Compact));
        // how many updates will enemy bot need to hit my bot
        // average 4 updates per second
        double enemySpeedPerUpdate = enemyBot["speed"].toInt()/4.;

        // if speed = 0 then speed per update is 0 so we need to "simulate" infinite
        // number of updates to rotate towards the enemy
        int updatesNumber       = (enemySpeedPerUpdate == 0) ? 99 : botDistance/enemySpeedPerUpdate;

        // how many degrees it is needed to rotate to face enemy
        // first we need vector of our bot in direction he is facing and vector from
        // out bot to enemy bot to calculate angle distance
        Point myBotVector = facingEndPoint - myBotCenter;
        Point enemyVector = enemyBotCenter - myBotCenter;

        // signs are ok, because we measure angle from enemy to mybot so it is positive
        // when clockise and negative when anticlockwise, just same as the game orientation
        double angleDistance = Conflict::angle(enemyVector, myBotVector);

        //check if i can rotate toward bot in number of updates enemy
        // needs to hit me
        int mySpeed = _myBot["speed"].toInt();
        int maxAngleRotation = _speedLevels[mySpeed];

        if(maxAngleRotation * updatesNumber > abs(angleDistance))
        {
            // compute rotation angle
            double rotationAngle = (abs(angleDistance) > maxAngleRotation) ? maxAngleRotation : angleDistance;

            // if maxAngleRotation, check sign
            rotationAngle = (angleDistance < 0 && rotationAngle > 0) ? -rotationAngle : rotationAngle;

            // rotate
            _command.insert("cmd", "steer");
            _command.insert("angle", rotationAngle);

            // return successful defend
            return 0;
        }
        else
        {
            // it is faster to check defend strategy for slowest speed
            // to determine if it is possible to defend, than to compute number of
            // updates needed to slow down

            // we already checked defend strategy for current speed
            // assume the current speed is fastest (must not be true)
            // then it is needed 4 updates to completely slow donw
            // if  bot is able to defend at slowest speed, it is able
            // to defend, so we just slow down and next update evaluation
            // will do the rest

            maxAngleRotation = _speedLevels[10];

            if(maxAngleRotation * (updatesNumber - 4) > abs(angleDistance))
            {
                _command.insert("cmd", "brake");

                return 0;
            }
            else
            {
                //unable to defend
                return 1;
            }
        }
    }
    else
    {
        // if facing, do nothing, just stay there and defend yourself
        return 0;
    }
}


