/**
 * @file    BotApplication.cpp
 * @brief   Main application that initializes all modules
 *          and takes care of error handling and communication
 *          between modules.
 *
 * @author Jaroslav Petrovsky
 * @date   18. April 2015
 */

#include "BotApplication.h"

#include "Game.h"
#include "TcpClient.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QCryptographicHash>
#include <QString>
#include <QCoreApplication>
#include <QTimer>
#include <QThread>

#include <iostream>

BotApplication::BotApplication( QCoreApplication* parentApp )
    : QObject(), _parentApp(parentApp), _game(new Game(this)), _tcpClient(new TcpClient(this))
{
    // ensure proper quit action
    QObject::connect(this, SIGNAL(quit()), _parentApp, SLOT(quit()));

    std::cout << "Ysarem's bot is loading ..." << std::endl;

    // setup client
    QObject::connect(_tcpClient, SIGNAL(error(ErrorLevel,QString)), SLOT(error(ErrorLevel,QString)));
    QObject::connect(_tcpClient, SIGNAL(messageReceieved(QByteArray)), SLOT(receiveMessage(QByteArray)));

    QObject::connect(this, SIGNAL(sendCommand(QByteArray)), _tcpClient, SLOT(sendGameUpdate(QByteArray)));
    QObject::connect(this, SIGNAL(sendLoginData(QByteArray)), _tcpClient, SLOT(sendLoginData(QByteArray)));

    _tcpClient->handshake("botwarz.eset.com", 8080);

    // setup game
    QObject::connect(_game, SIGNAL(sendCommand(QByteArray)), this, SLOT(processCommandFromGame(QByteArray)));
}

BotApplication::~BotApplication()
{
    // cleanup
    if(_tcpClient != nullptr)
    {
        delete _tcpClient;
        _tcpClient = nullptr;
    }

    if(_parentApp != nullptr)
    {
        _parentApp = nullptr;
    }

    if(_game != nullptr)
    {
        delete _game;
        _game = nullptr;
    }
}

void BotApplication::error(ErrorLevel errorLevel, QString errorMessage)
{
    if(errorLevel == ErrorLevel::FATAL)
    {
        std::cout << "FATAL " << errorMessage.toStdString() << std::endl;
        emit quit();
    }
    if(errorLevel == ErrorLevel::INFO)
    {
        std::cout << "INFO " << errorMessage.toStdString() << std::endl;
    }
}

// message from server processing
void BotApplication::receiveMessage(QByteArray message)
{
    QJsonDocument jsonDocument  = QJsonDocument::fromJson(message);
    QJsonObject jsonObject      = jsonDocument.object();

    // most importat actions first
    if(jsonObject["play"].isObject() == true)
    {
        // process lastCmdId
        if(jsonObject["play"].toObject()["lastCmdId"].isNull() == false)
        {
            QMutexLocker lock(&_commandLock);

            _lastProcessedCmdId = jsonObject["play"].toObject()["lastCmdId"].toString().toInt();
        }

        _game->update(jsonObject);

    }
    else if(jsonObject["game"].isNull() == false)
    {
        QJsonArray speedLevelsArray = jsonObject["game"].toObject()["speedLevels"].toArray();
        QMap<int, int> speedLevelsMap;

        for(int i = 0; i < speedLevelsArray.size(); i++)
        {
            QJsonObject speedLevel = speedLevelsArray[i].toObject();

            speedLevelsMap.insert(speedLevel["speed"].toInt(), speedLevel["maxAngle"].toInt());
        }

        _game->start(NICKNAME, speedLevelsMap);


        std::cout << "Game has begun ... " << std::endl;
    }
    else if(jsonObject["result"].isNull() == false)
    {
        std::cout << "Game result arrived: ";
        std::cout << jsonObject["result"].toObject()["status"].toString().toStdString() << std::endl;
        std::cout << "================================================================" << std::endl;

        _game->cleanUp();

        resetApp();
    }
    else
    {
        // process to login
        if(jsonObject["status"] == "socket_connected")
        {
            std::cout << "Handshake successful. Connection with server established. Proceding to login." << std::endl;

            // hash generation
            QByteArray hash = generateHash(jsonObject["random"].toString());
            QString hashText(hash.toHex());

            // json object creation
            QJsonObject loginObject     = QJsonObject();
            QJsonObject loginDataObj    = QJsonObject();

            // json objects fill
            loginDataObj.insert("nickname", QJsonValue(NICKNAME));
            loginDataObj.insert("hash",     QJsonValue(hashText));

            loginObject.insert("login",     QJsonValue(loginDataObj));

            // construct and send raw data
            QByteArray rawData = QJsonDocument(loginObject).toJson(QJsonDocument::Compact);

            emit sendLoginData(rawData);
        }
        else if(jsonObject["status"] == "login_ok")
        {
            std::cout << "Login successful. Waiting for data to start the game ..." << std::endl;
        }
        else
        {
            std::cout << QString(QJsonDocument(jsonObject).toJson(QJsonDocument::Compact)).toStdString() << std::endl;
            std::cout << QString(message).toStdString() << std::endl;
        }
    }
}

void BotApplication::processCommandFromGame(QByteArray command)
{
    QMutexLocker lock(&_commandLock);

    // keep the interval
    if(_isCommandInQueue == false)
    {
//        _delayTimer.restart();
        QTimer::singleShot(DELAY, this, SLOT(prepareCommand()));
    }

    // update queued command
    _queuedCommand = QJsonDocument::fromJson(command).object();
    _isCommandInQueue = true;
}

QByteArray BotApplication::generateHash(QString random)
{
    QString text = random + TOKEN;
    QByteArray hash = QCryptographicHash::hash(text.toLatin1(), QCryptographicHash::Sha1);

    return hash;
}

int BotApplication::generateCmdId()
{
    // random four digit number
    return ( qrand() % (9000) + 1000);
}

// app cleanup after match
void BotApplication::resetApp()
{
    QMutexLocker lock(&_commandLock);
    _queuedCommand = QJsonObject();
    _isCommandInQueue = false;

    _lastProcessedCmdId = -1;
    _lastSentCmdId      = -1;
}

void BotApplication::prepareCommand()
{
//    // keep the interval double check
//    if(_delayTimer.elapsed() < DELAY)
//    {
//        QThread::msleep(DELAY - _delayTimer.elapsed());
//    }

    QMutexLocker lock(&_commandLock);

    if(_lastProcessedCmdId == _lastSentCmdId)
    {
        // if the game receieved result update and the app had reset in the meantime
        // the timer has triggered
        if(_queuedCommand.isEmpty() == false)
        {
            _lastSentCmdId = generateCmdId();

            _queuedCommand.insert("cmdId", QJsonValue(QString::number(_lastSentCmdId)));

            emit sendCommand(QJsonDocument(_queuedCommand).toJson(QJsonDocument::Compact));

            _isCommandInQueue = false;
        }
    }
    else
    {
        // if the server didnt get our last command, reste queue and process another one
        _isCommandInQueue = false;
    }
}
