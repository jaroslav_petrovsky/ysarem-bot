# README #

This application is an implementation of https://botwarz.eset.com/#!/ bot. Below follows copied text from this page in case it gets down in future.

The bot AI is not finished.

### How to play ###

https://botwarz.eset.com/#!/howto

**ESET BotWaRz**

The matches you see on this page are part of the game played by BOT programs developed by other programmers. If you want to take the challenge and test your skills against theirs, go ahead and write your own BOT! All you need is a simple client application that is able to send commands and receive game updates through TCP socket in the JSON format. The application was designed to run on your computer so you can watch the matches on our web. Whether you succeed depends entirely on the effectiveness of your code (artificial intelligence) that drives your BOTs.
Send us your code
The choice of platform and programming language is up to you. However, if you want to impress ESET development team leaders and potentially receive a job offer, send us your code in the programming language and for the platform used by the department that strikes your fancy, as presented here. It's not your game score that matters. So do not hesitate to send in your code, even if there are more successful bots. We are interested in your source code structure and how effectively you can use the programming language.
Sign up and get your login token
Sign up here
Enter your nickname and email
You will receive your login details by email
Rules

**Game Basics**

One on one duel where each player has 5 individually controlled bots. Accelerate, brake and steer your bots with commands over TCP socket. Chase your opponents’ bots and hit them from rear or side to destroy them. Be careful not to destroy your own bots. The faster you drive, the less you can steer. You can send a command to each bot every 200ms. The game ends when one player’s bots are all destroyed or when the time limit of 60 seconds is reached. In this case, the player with more bots alive wins.

**World**

900 x 600 pixels (x0, y0 in the top left corner)
Game time in milliseconds.
Speed in pixels per second.
0° angle is heading EAST and increases clockwise.

**Bots**

Bot is represented as a circle with 20px radius R with center on coordinates X, Y.
Bot has speed defined in pixels per second and angle of direction in degrees.
Bot has a front bumper which is used to destroy other bots - 90° angle arc of the circle.
Collision is detected when two bots overlap (the distance between their centers is smaller than 2*R).

**Possible Collision Types**

Bot bumper vs. bot body – the latter bot is destroyed.
Bot bumper vs. bot bumper – both bots bounce off.
Bot body vs. bot body – both bots bounce off.
Bot vs. wall – bot is stuck until it is turned around, away from the wall so it can move freely again.

**Moving Around**

Bot moves at one of 5 speeds that are predefined by the game server at the beginning of a game. If you “speedup/brake,” your bot starts to move at higher/lower predefined speed immediately (no acceleration or deceleration is computed). Bot can never stop completely (if the bot is moving at the lowest speed, the “brake” command will be ignored). You can steer your bot by specifying an angle that will be added to current bot’s direction. Each speed has a maximum steering angle associated with it - the faster you go, the lower the maximum angle is (e.g. at the lowest speed, you move at 20px/s and can steer by ±30° at a time but at the highest speed (360px/s) you can only steer by ±2°).

**Game Server**

The matches are hosted on an ESET game server. You write a TCP/IP client that receives map updates and sends commands to your bots
The matches are visualized in your browser when you visit the game server TCP/IP protocol. Use TCP socket to send commands and receive game updates from server. Use minified JSON to transmit data (no whitespace). The examples on this page are formatted to increase readability. Do not use \n inside JSON string (see below). Use delimited message framing - all messages must end with '\n' delimiter (Line delimited JSON).

**TCP server**

botwarz.eset.com:8080 or botwarz.eset.com:2000, both ports point to the same game server.

**Stream example**

```
#!JSON

{"login":{"hash":"6ccc1024f27c75e46c5d6f277b4a46ac664b4f14","nickname":"YourNick"}}
{"bots":[{"id":2,"cmd":"steer","angle":-5}]}
{"bots":[{"id":2,"cmd":"brake"},{"id":4,"cmd":"accelerate"}]}
```


**Connect to socket and log in**

The game uses challenge-response authentication.
Server sends a random string in the welcome message.
Take the random string from server, append your token, create a SHA-1 hash of the result: hash=SHA1(random+token).
Send the hash back along with your nick to authenticate.

RECEIVE Welcome message after connecting to socket with random string

```
#!JSON

{
   "status": "socket_connected",
   "msg":    "Hello there! You are connected to BotWaRz game server.",
   "random": "d039abeb6ba92bf6c8798c01d30668ff424ef1208bdcc1cc33af438b2d1c62c2"
}
```
Use SHA-1 hash of random string from server and your token to login

C++ example:

```
#!c++

...
const char* token = "10fc436a-4ca9-48b3-991a-d088cd753c15";
...
std::string hash = GenerateHashValue(json["random"].asString().c_str(), token);
...
std::string GenerateHashValue(const char *random, const char *myToken)
{
	std::string text = random;
        text += myToken;

	unsigned char buffer[20];
	sha1::calc(text.c_str(), text.size(), buffer);

        char ret[41];
	sha1::toHexString(buffer, ret); // use lowercase letters!

	return ret;
}
```




SEND Write your login data to socket

```
#!JSON

{
   "login":{
       "nickname":"Jimmy",
       "hash":"bdf4e6e466462c23d203e245b66bdb0b58d8b939"
   }
}
```
RECEIVE Login successful

```
#!JSON

{
   "status": "login_ok",
   "msg":"Login was successful. Please wait, you will receive initial game data when game starts."
}
```
**Game start**

When the game starts you will receive initial game data.
You can identify your bots by nickname.
Speed levels define maximum allowed steering angles of your bots at different speeds.


RECEIVE Game started

```
#!JSON

{
    "game": {
        "time": 0,
        "botRadius": 20,
        "world":{
            "width":900,
            "height":600,
        },
        "speedLevels":[
            { "speed": 20, "maxAngle": 30 },
            { "speed": 50, "maxAngle": 15 },
            { "speed": 90, "maxAngle": 10 },
            { "speed": 180, "maxAngle": 5 },
            { "speed": 360, "maxAngle": 2 }
        ],
        "players":[
            {
                "nickname": "Johnny",
                "asset": "fire",
                "bots": [
                    { "id": 1, "x": 100, "y": 100, "angle": 0, "speed": 20 },
                    { "id": 2, "x": 100, "y": 200, "angle": 0, "speed": 20 },
                    { "id": 3, "x": 100, "y": 300, "angle": 0, "speed": 20 },
                    { "id": 4, "x": 100, "y": 400, "angle": 0, "speed": 20 },
                    { "id": 5, "x": 100, "y": 500, "angle": 0, "speed": 20 }
                ]
            },
            {
                "nickname": "Jimmy",
                "asset": "spark",
                "bots": [
                    { "id": 6, "x": 932, "y": 100, "angle": 180, "speed": 20 },
                    { "id": 7, "x": 932, "y": 200, "angle": 180, "speed": 20 },
                    { "id": 8, "x": 932, "y": 300, "angle": 180, "speed": 20 },
                    { "id": 9, "x": 932, "y": 400, "angle": 180, "speed": 20 },
                    { "id": 10, "x": 932, "y": 500, "angle": 180, "speed": 20 }
                ]
            }
        ]
    }, 
}
```

**Moving Around**

You can accelerate, brake or steer your bots by commands.
You can send 1 command to each bot in one message.
Minimal interval between messages must be at least 200ms (penalties may apply if violated).
On first two violations of the interval you get a warning and the command is discarded.
On third violation you get disconnected.

**Keeping The Interval**

Even if you send two commands 200ms apart, it is not guaranteed that server will receive them 200ms apart.
Some packets may get delayed during network transmission and you can get violations even if you spaced commands properly.
In each command you can specify command ID (cmdId) - it can be a command sequence number, random string, etc.
In game status updates, the server will include the latest cmdId seen from the player.
This way you get a confirmation that server processed your command and you can be sure you won't violate the interval if you send next command 200ms later.


SEND Move your bots

```
#!JSON

{
   "cmdId":1234,   
   "bots":[
      { "id":1, "cmd":"steer", "angle": 10 },
      { "id":3, "cmd":"accelerate" },
      { "id":4, "cmd":"brake" },
      { "id":5, "cmd":"steer", "angle": -3 }
   ]
}
```
RECEIVE You may receive response with error when processing of command wasn't successful

```
#!JSON

{
   status: "command_error_json",
   msg:"I was unable to parse your command (not a valid JSON). Your connection has been terminated!"
}

{
   status: "command_time_error",
   msg:"Delay between commands have to be at least 200ms (your delay was 180ms). You've been penalized..."
}
```

**Game Updates**

The game server sends data about all surviving bots. The updates are triggered by these events:
Change of direction or speed.
Collision between bots.
Collision of a bot with world boundaries.
After receiving a command with cmdId field present.

Please note that the angle might be negative (the current range sent by server is -180 to 180).

RECEIVE Game update

```
#!JSON

{
    "play":{
        "time":1931,
        "players":[
            {
                "nickname": "Johnny",
                "bots":[
                    { "id":1, "x":310.28, "y":48.24, "angle":-45, "speed": 20},
                    { "id":2, "x":289.54, "y":301.82, "angle":45, "speed": 20},
                    { "id":3, "x":331.72, "y":300, "angle":0, "speed": 50},
                    { "id":4, "x":331.72, "y":400, "angle":0, "speed": 180},
                    { "id":5, "x":276.86, "y":529, "angle":45, "speed": 360}
                ]
            },
            {
                "nickname": "Jimmy",
                "bots":[
                    { "id":1, "x":700.28, "y":100, "angle":180, "speed": 50},
                    { "id":2, "x":717.15, "y":159.27, "angle":180, "speed": 180},
                    { "id":5, "x":762.88, "y":484.88, "angle":-135, "speed": 20}
                ]
            },
        ]
       "lastCmdId":1234
   }
}
```
**Game result**

When the game ends you will receive the results.

RECEIVE Game result

```
#!JSON

{
   "result": {
        "time": 9986,
        "status": "Johnny won!",
        "winner": {
            "nickname": "Johnny",
            "bots": [
                { "id": 1, "x": 697.28, "y": 72, "angle": -180},
                { "id": 2, "x": 972.55, "y": 243.43, "angle": -135},
                { "id": 5, "x": 970.65, "y": 277.54, "angle": -180 }
            ]
        }
    } 
}
```